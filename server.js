// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// -------------------------------------------- Linked IN -----------------------------

// var Linkedin = require('node-linkedin')('api', 'secret', 'callback');

// // otherwise you can leave `res` out, and the module will respond with the redirect url
// Linkedin.auth.authorize(['r_basicprofile', 'r_fullprofile', 'r_emailaddress', 'r_network', 'r_contactinfo', 'rw_nus', 'rw_groups', 'w_messages']);

// // Again, `res` is optional, you could pass `code` as the first parameter
// app.get('/oauth/linkedin/callback', function(req, res) {

//   console.log("Got response: " + res.statusCode);
//   Linkedin.auth.getAccessToken(res, req.query.code, function(err, results) {
//     if ( err )
//       return console.error(err);

//     /**
//      * Results have something like:
//      * {"expires_in":5184000,"access_token":". . . ."}
//      */

//     console.log(results);
//     return res.redirect('/');
//   });
// });


// var myAccessToken = 'AQXQE7HcLtu0jmzloQbjIrbQVw3MpTPj_e9fkpA4Txm8qRJzddx9A-OZDauGL5BYQhpPi6bNWJ9r8pEkIKmxicWLtq1K0bUG_Za8Phy3JAga50MP9bLNVjFJ8ndtlx7WVoNpoSZwJTcIRgkLTipTITgR7UAl9jQzQ703pwJyYPOaH-_fxGw';

// var linkedin = Linkedin.init(myAccessToken, {
//   timeout: 10000 /* 10 seconds */
// });
// // Using a library like `expressjs` the module will
// // redirect for you simply by passing `res`.
// app.get('/oauth/linkedin', function(req, res) {
//   // This will ask for permisssions etc and redirect to callback url.
//   Linkedin.auth.authorize(res, ['r_basicprofile', 'r_fullprofile', 'r_emailaddress', 'r_network', 'r_contactinfo', 'rw_nus', 'rw_groups', 'w_messages']);
// });


// linkedin.people.me(['id', 'first-name', 'last-name'], function(err, $in) {
//   // Loads the profile of access token owner.
//   console.log($in);
// });
// -------------------------------------------- Linked IN -----------------------------

// -------------------------------------------- Twitter -----------------------------
var Twitter = require('node-twitter');

var twitterRestClient = new Twitter.RestClient(
    'y7iIkgyIsCxlOKvgEhDXVHVGU',
    'xvlkbZp1tx4WE6RCRc1vlK1hnvQdhzOcSc2T8brbdfE3WYiSW2',
    '1418399964-ZFpUy38IJ4biwe5ctnREuGfIydtTDLneOnUTDye',
    'Tls9gechFU42o9f9LR2Z6eGXlqt7JNbXZt5BOa3oouk7Q'
);

// twitterRestClient.statusesHomeTimeline({}, function(error, result) {
//   if (error)
//   {
//     console.log('Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
//   }

//   if (result)
//   {
//     console.log(result);
//   }
// });


// twitterRestClient.search("genesys", function(error, result) {
//   if (error)
//   {
//     console.log('Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
//   }

//   if (result)
//   {
//     console.log(result);
//   }
// });

// var searchUser = "Vikas Kumar";
var searchUser = "Clive John Chuan";
var userTweets = [];

var twitterSearchClient = new Twitter.SearchClient(
    'y7iIkgyIsCxlOKvgEhDXVHVGU',
    'xvlkbZp1tx4WE6RCRc1vlK1hnvQdhzOcSc2T8brbdfE3WYiSW2',
    '1418399964-ZFpUy38IJ4biwe5ctnREuGfIydtTDLneOnUTDye',
    'Tls9gechFU42o9f9LR2Z6eGXlqt7JNbXZt5BOa3oouk7Q'
);
// twitterSearchClient.search({'q': 'everest'}, function(error, result) {
//     if (error)
//     {
//         console.log('Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
//     }

//     if (result)
//     {
//         console.log(result);
//     }
// });
twitterSearchClient.search({"q":"genesys"}, function(error, result) {
  if (error)
  {
    console.log('Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
  }

  if (result)
  {
    // console.log(result);
    if (result && result.statuses) {
      result.statuses.forEach(function (tweet) {
        var user = tweet.user;
        console.log("user name: " + user.name);
        if (user && user.name == searchUser) {
          console.log("found user: " + user.name);
          userTweets.push(tweet.text);
          var tweets = "";
          userTweets.forEach( function (tweet, idx) {
            if (idx > 0) {
              tweets += ", ";
            }
            tweets += tweet;
          });
          console.log("Tweets by " + searchUser + ": " + tweets);
        }
      });
    }
  }
});

// -------------------------------------------- Twitter -----------------------------

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
  res.json({ message: 'hooray! welcome to our api!' });
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);